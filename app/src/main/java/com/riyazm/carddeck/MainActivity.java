package com.riyazm.carddeck;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    ArrayList<CardPoJo> itemsList = new ArrayList<>();
    List<Integer> picturesList = Arrays.asList(R.drawable.calculator, R.drawable.chrome,
            R.drawable.google_plus, R.drawable.news_stand, R.drawable.play_store);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        itemsList.add(new CardPoJo("Title1", getRandomPicture()));
        itemsList.add(new CardPoJo("Title2", getRandomPicture()));
        itemsList.add(new CardPoJo("Title3", getRandomPicture()));
        itemsList.add(new CardPoJo("Title4", getRandomPicture()));
        itemsList.add(new CardPoJo("Title5", getRandomPicture()));
        itemsList.add(new CardPoJo("Title6", getRandomPicture()));

        RecentsList recents = (RecentsList) findViewById(R.id.recents);
        recents.setAdapter(new RecentsAdapter() {
            @Override
            public String getTitle(int position) {
                CardPoJo cardPoJo = itemsList.get(position);
                return cardPoJo.text;
            }

            @Override
            public View getView(int position) {
                ImageView iv = new ImageView(MainActivity.this);
                iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
                CardPoJo cardPoJo = itemsList.get(position);
                iv.setImageResource(cardPoJo.drawable);
                iv.setBackgroundColor(0xffffffff);
                return iv;
            }

            @Override
            public Drawable getIcon(int position) {
                return getResources().getDrawable(R.mipmap.ic_launcher);
            }

            @Override
            public int getHeaderColor(int position) {
                return generateRandomColor();
            }

            @Override
            public int getCount() {
                return itemsList.size();
            }
        });
        recents.setOnItemClickListener(new RecentsList.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Log.d(TAG, "clicked: " + position);
            }
        });
    }

    private int generateRandomColor() {
        Random random = new Random();
        int red = random.nextInt(150);
        int green = random.nextInt(150);
        int blue = random.nextInt(150);

        return Color.rgb(red, green, blue);
    }

    private int getRandomPicture() {
        Random random = new Random();
        int index = random.nextInt(picturesList.size());
        return picturesList.get(index);
    }
}
