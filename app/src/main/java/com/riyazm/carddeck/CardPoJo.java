package com.riyazm.carddeck;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by muhammadriyaz on 06/04/17.
 */

public class CardPoJo {
    public String text;
    public int drawable;


    public CardPoJo(String text, int drawable) {
        this.text = text;
        this.drawable = drawable;
    }


    @Override
    public String toString() {
        return "CardPoJo{" +
                "text='" + text + '\'' +
                '}';
    }
}
