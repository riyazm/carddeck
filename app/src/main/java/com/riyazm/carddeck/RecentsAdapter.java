package com.riyazm.carddeck;

import android.graphics.drawable.Drawable;
import android.view.View;

/**
 * Created by muhammadriyaz on 06/04/17.
 */

public interface RecentsAdapter {
    String getTitle(int position);

    View getView(int position);

    Drawable getIcon(int position);

    int getHeaderColor(int position);

    int getCount();
}